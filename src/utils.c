#include <time.h>
#include <stdio.h>
#include <netdb.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <unistd.h>
#include "utils.h"

void read_data(int sock, void* dest, size_t size)
{
	ssize_t n = read(sock, dest, size);
	
	if (n < 0) {
		perror("ERROR reading from socket");
		exit(1);
	}
}

void write_data(int sock, const void* data, size_t size)
{
	ssize_t n = write(sock, data, size);
	
	if (n < 0)
	{
		perror("ERROR writing to socket");
		exit(1);
	}
}


int get_time(char* dest)
{
	time_t rawtime;
	time (&rawtime);

	struct tm * timeinfo = localtime (&rawtime);
	return strftime (dest, TIME_FORMAT_LEN, "%c",timeinfo);
}

const char* get_city()
{
	static const char* city = CITY;
	return city;
}

const char* get_scale(request_type request)
{
	static const char* scale[2] = {SCALE_CELSIUS, SCALE_FAHRENHEIT}; 
	switch (request)
	{
		case REQUEST_TYPE_CELSIUS:
		case REQUEST_TYPE_FAHREHNHEIT:
			return scale[request];
		default:
			fprintf(stderr, "Invalid request type %d\n", request);
			return NULL;
	}
}

int get_temperature()
{
	srand(time(NULL));
	return TEMPERATURE_MIN + (double)rand()/RAND_MAX * (TEMPERATURE_MAX - TEMPERATURE_MIN);
}

void make_response(char* dest, request_type request)
{
	if ( REQUEST_TYPE_INVALID >= request || request >= REQUEST_TYPE_MAX)
	{
		fprintf(stderr, "Invalid request type %d\n", request);
		snprintf(dest, DATA_SIZE, "Invalid request");
		return;
	}

	snprintf(dest, 
		DATA_SIZE, 
		"%s is at %d %s at ", 
		get_city(),
		get_temperature(), 
		get_scale(request)
	);

	get_time(dest + strlen(dest));
}
