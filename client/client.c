#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include "client.h"
#include "message.h"
#include "utils.h"

/**
 * Init connection
 */
int init_client(const char* host, int port)
{
	int sockfd = socket(AF_INET, SOCK_STREAM, 0);

	if (sockfd < 0)
	{
		perror("Failed to open socket");
		return 1;
	}

	struct hostent *server = gethostbyname(host);
	if (server == NULL)
	{
		fprintf(stderr,"ERROR, no such host\n");
		return 1;
	}

	struct sockaddr_in serv_addr;
	bzero((char *) &serv_addr, sizeof(serv_addr));
	serv_addr.sin_family = AF_INET;
	bcopy((char *)server->h_addr, (char *)&serv_addr.sin_addr.s_addr, server->h_length);
	serv_addr.sin_port = htons(port);

	if (connect(sockfd,(struct sockaddr *) &serv_addr,sizeof(serv_addr)) < 0) 
	{
		perror("ERROR connecting");
		return 1;
	}
	return sockfd;
}

/**
 * Destroy connection
 */
void destroy_client(int client)
{
	close(client);
}

/**
 * Send client request
 */
void send_request(int client)
{
	struct t_request request;
	request.type = REQUEST_TYPE_FAHREHNHEIT;
	write_data(client, &request, sizeof(request));
}

/**
 * Recieve server response
 */
void recv_response(int client, struct t_response* response)
{
	size_t size = sizeof(*response);
	bzero(response, size);
	read_data(client, response, size);
}

/**
 * Display server response
 */
void print_response(const struct t_response* response)
{
	printf("%s\n", response->data);
}

/**
 * Send request and process response
 */
int client_run(const char* host, int port)
{
	struct t_response response;
	int client = init_client(host, port);

	send_request(client);
	recv_response(client, &response);
	print_response(&response);

	destroy_client(client);

	return 0;
}
