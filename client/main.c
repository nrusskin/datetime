#include <stdio.h>
#include <stdlib.h>
#include "client.h"

void printUsage(char* cmd)
{
	printf("Usage:\t%s <server> <port>\n", cmd);
}

int main(int argc, char* argv[])
{
	if (argc < 3)
	{
		printUsage(argv[0]);
		return -1;
	}

	return client_run(argv[1], atoi(argv[2]));
}
