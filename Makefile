COMPONENT=client server
COMPONENTS=$(COMPONENT:%=--directory=%)

.PHONY: all

all: ${COMPONENTS}

$(COMPONENTS):
	$(MAKE) $@
