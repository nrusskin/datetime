#include <stdio.h>
#include <netdb.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <unistd.h>

#include "server.h"
#include "message.h"
#include "utils.h"

#define PORT (24459)
#define SERVER "localhost"

/**
 * @return requested scale type (Celsius/Fahrenheit)
 */
request_type recv_request(int sock)
{
	struct t_request request;

	read_data(sock, (void*)&request, sizeof(request));
	return request.type;
}

/**
 * send response to the client
 */
void send_response(int sock, request_type request)
{
	struct t_response response;

	make_response(response.data, request);
	write_data(sock, (void*)&response, sizeof(response));
}

/**
 * Handle client request
 */
void process_request(int sock)
{
	request_type request = recv_request(sock);
	send_response(sock, request);
}

int server_run()
{
	int sockfd = socket(AF_INET, SOCK_STREAM, 0);
	if (sockfd < 0)
	{
		perror("Failed to open socket");
		return 1;
	}

	struct sockaddr_in serv_addr, cli_addr;
	bzero((char *) &serv_addr, sizeof(serv_addr));
	serv_addr.sin_family = AF_INET;
	serv_addr.sin_addr.s_addr = INADDR_ANY;
	serv_addr.sin_port = htons(PORT);
  
	if (bind(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0)
	{
		perror("Failed to binding port");
		return 1;
	}

	listen(sockfd,5);
	socklen_t clilen = sizeof(cli_addr);

	printf("EDMTS is running on %s, listening on port %d\n", SERVER, PORT);

	while (1) {
		
		int newsockfd = accept(sockfd, (struct sockaddr *) &cli_addr, &clilen);
		if (newsockfd < 0)
		{
			perror("ERROR on accept");
			return 1;
		}
		
		/* Create child process */
		pid_t pid = fork();
		if (pid < 0)
		{
			perror("ERROR on fork");
			return 1;
		}
		
		if (pid == 0)
		{
			/* This is the client process */
			close(sockfd);
			process_request(newsockfd);
			return 0;
		}
		else
		{
			close(newsockfd);
		}
		
	}

	close(sockfd);

	return 0;
}
