#ifndef __UTILS_H__
#define __UTILS_H__

#include "message.h"

#define CITY "Edmonton"

#define SCALE_CELSIUS "Celsius"
#define SCALE_FAHRENHEIT "Fahrenheit"

#define TIME_FORMAT_LEN (32)

#define TEMPERATURE_MAX (50)
#define TEMPERATURE_MIN (-10)

/**
 * Write formated date/time string to dest
 */
int get_time(char* dest);

/**
 * @return default city (Edmonton)
 */
const char* get_city();

/**
 * @return Scale type (Celsius/Fahrenheit) by request type
 */
const char* get_scale(request_type request);

/**
 * @return random value
 */
int get_temperature();

/**
 * read data from the socket
 */
void read_data(int sock, void* dest, size_t size);

/**
 * write data to the socket
 */
void write_data(int sock, const void* data, size_t size);

/**
 * Build client response
 */
void make_response(char* dest, request_type request);

#endif//__UTILS_H__
