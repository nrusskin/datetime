#ifndef __MESSAGE_H__
#define __MESSAGE_H__

#define DATA_SIZE (64)

typedef enum {
	REQUEST_TYPE_INVALID		= -1,
	REQUEST_TYPE_CELSIUS		= 0,
	REQUEST_TYPE_FAHREHNHEIT	= 1,
	REQUEST_TYPE_MAX			= 2
} request_type;

struct t_request
{
	request_type type;	
};

struct t_response
{
	char data[DATA_SIZE];
};

#endif//__MESSAGE_H__
